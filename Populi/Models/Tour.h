//
//  Tour.h
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tour : NSObject
@property (strong) NSString* uid;
@property (strong) NSString* title;
@property (strong) NSString* image;
@property (strong) NSString* desc;
- (instancetype)initWithJsonDict:(NSDictionary*) jsonDict;
@end
