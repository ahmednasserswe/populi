//
//  Tour.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "Tour.h"

@implementation Tour
@synthesize uid,title,image,desc;
- (instancetype)initWithJsonDict:(NSDictionary*) jsonDict
{
    self = [super init];
    if (self) {
        self.uid = [jsonDict objectForKey:@"uid"];
        self.title = [jsonDict objectForKey:@"title"];
        self.image = [jsonDict objectForKey:@"image"];
        self.desc = [jsonDict objectForKey:@"desc"];
    }
    return self;
}

@end
