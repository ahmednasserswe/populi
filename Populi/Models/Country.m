//
//  Country.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "Country.h"
#import "City.h"
@implementation Country
@synthesize _id,name,cities;
- (instancetype)initWithJsonDict:(NSDictionary*) jsonDict
{
    self = [super init];
    if (self) {
        self._id = [jsonDict objectForKey:@"id"];
        self.name = [jsonDict objectForKey:@"name"];
        NSMutableArray* citiesFromJson = [NSMutableArray new];
        for (NSDictionary* cityJson in [jsonDict objectForKey:@"cities"]) {
            City* city = [[City alloc] initWithJsonDict:cityJson];
            [citiesFromJson addObject:city];
        }
        self.cities = [NSArray arrayWithArray:citiesFromJson];
    }
    return self;
}
@end
