//
//  City.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "City.h"
#import "Tour.h"
@implementation City
@synthesize _id,name,tours;

- (instancetype)initWithJsonDict:(NSDictionary*) jsonDict
{
    self = [super init];
    if (self) {
        self._id = [jsonDict objectForKey:@"id"];
        self.name = [jsonDict objectForKey:@"name"];
        NSMutableArray* toursFromJson = [NSMutableArray new];
        for (NSDictionary* tourJson in [jsonDict objectForKey:@"tours"]) {
            Tour* tour = [[Tour alloc] initWithJsonDict:tourJson];
            [toursFromJson addObject:tour];
        }
        self.tours = [NSArray arrayWithArray:toursFromJson];
    }
    return self;
}
@end
