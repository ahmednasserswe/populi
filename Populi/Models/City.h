//
//  City.h
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject
@property (strong) NSString* _id;
@property (strong) NSString* name;
@property (strong) NSArray* tours;
- (instancetype)initWithJsonDict:(NSDictionary*) jsonDict;
@end
