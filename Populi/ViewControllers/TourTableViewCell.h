//
//  TourTableViewCell.h
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tour.h"
@interface TourTableViewCell : UITableViewCell
- (void) bind:(Tour*) tour;
@end
