//
//  TourDetailsViewController.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "TourDetailsViewController.h"
#import <UIImageView+AFNetworking.h>
@interface TourDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *tourImageView;
@property (weak, nonatomic) IBOutlet UILabel *tourTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *tourDescTextView;

@end

@implementation TourDetailsViewController
@synthesize tour;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tourImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://staticfiles.popguide.me/code_challenge/%@",self.tour.image]]];
    self.tourTitleLabel.text = self.tour.title;
    self.tourDescTextView.text = self.tour.desc;
    self.title = self.tour.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
