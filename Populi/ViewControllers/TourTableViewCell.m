//
//  TourTableViewCell.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "TourTableViewCell.h"
#import "Tour.h"
#import <UIImageView+AFNetworking.h>
@interface TourTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *tourImageView;
@property (weak, nonatomic) IBOutlet UILabel *tourTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tourDescLabel;

@end

@implementation TourTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) bind:(Tour*) tour
{
    self.tourTitleLabel.text = tour.title;
    self.tourDescLabel.text = tour.desc;
    [self.tourImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://staticfiles.popguide.me/code_challenge/%@",tour.image]]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
