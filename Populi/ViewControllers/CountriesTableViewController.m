//
//  CountriesTableViewController.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "CountriesTableViewController.h"
#import "CountriesManager.h"
#import "Country.h"
#import "CitiesTableViewController.h"
#import <SVProgressHUD.h>
@interface CountriesTableViewController ()
@property (strong) NSArray* countries;
@property (strong) NSIndexPath* selectedIndexPath;
@end

@implementation CountriesTableViewController
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"countryCell"];
//    self.hud = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
//    self.hud.mode = MBProgressHUDModeAnnularDeterminate;
//    self.hud.label.text = @"Loading";
    [self loadFeed];
}
- (void) loadFeed
{
    self.countries = [NSArray new];
    [self.tableView reloadData];

    [SVProgressHUD show];
    [CountriesManager getCountries:^(NSArray *countries)  {
//        [self.hud hideAnimated:YES];
        [SVProgressHUD dismissWithDelay:1 completion:^{
            self.countries = countries;
            [self.tableView reloadData];

        }];

    } withError:^(NSError *error) {
        [SVProgressHUD dismiss];
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:error.localizedDescription
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
- (IBAction)refreshFeed:(id)sender {
    [self loadFeed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.countries.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"countryCell" forIndexPath:indexPath];
    cell.textLabel.text = [[self.countries objectAtIndex:indexPath.row]name];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"showCitiesTableViewCont" sender:self];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showCitiesTableViewCont"])
    {
        Country* country = [self.countries objectAtIndex:self.selectedIndexPath.row];
        if ([segue.destinationViewController isKindOfClass:[CitiesTableViewController class]]) {
            [(CitiesTableViewController*)(segue.destinationViewController) setCounty:country];
        }
    }
}


@end
