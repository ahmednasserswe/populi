//
//  CountriesManager.h
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountriesManager : NSObject
+(void) getCountries:(void (^)(NSArray* countries))success withError:(void (^)(NSError* error))error;
@end
