
//
//  CountriesManager.m
//  Populi
//
//  Created by Ahmed Nasser on 11/4/17.
//  Copyright © 2017 Ahmed Nasser. All rights reserved.
//

#import "CountriesManager.h"
#import <AFNetworking.h>
#import "Country.h"
@implementation CountriesManager

+(void) getCountries:(void (^)(NSArray* countries))success withError:(void (^)(NSError* error))error
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://staticfiles.popguide.me/code_challenge/countries_v1.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableArray* countriesArray = [NSMutableArray new];
        for (NSDictionary* countryJson in responseObject) {
            Country* country = [[Country alloc] initWithJsonDict:countryJson];
            [countriesArray addObject:country];
        }
//        [self saveCountries:responseObject];
        success(countriesArray);
    } failure:^(NSURLSessionTask *operation, NSError *errorObject) {
        NSLog(@"Error: %@", errorObject);
        error(errorObject);
    }];
}


@end
